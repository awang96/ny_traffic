# NYTraffic

This project is based on New York City Taxi Trip Duration train
data available on https://www.kaggle.com/c/nyc-taxi-trip-duration/data.

## Table of contents
1. [Installation](#installation-a-nameinstallationa)
2. [Usage](#usage-a-nameusagea)
3. [Example](#example-a-nameexamplea)

## Installation <a name="installation"></a>
Dependencies and build are managed with Poetry.  
Please visit https://python-poetry.org/docs/ to install Poetry.

```bash
poetry build
pip install dist/nytraffic*.whl
```

## Usage <a name="usage"></a>
### Compute average speed for each trip
```python
from nytraffic.transformation import with_average_speed

df_with_average_speed = with_average_speed(df)
```

### Compute average number of trips for each day of the week
```python
from nytraffic.transformation import get_trips_by_day

trips_by_day = get_trips_by_day(df)
# Days are represented by integers :
# 0 -> Monday
# 1 -> Tuesday
# 2 -> Wednesday
# 3 -> Thursday
# 4 -> Friday
# 5 -> Saturday
# 6 -> Sunday
```

### Compute average number of trips for 4 hours time slots
```python
from nytraffic.transformation import get_trips_by_time_slot

trips_by_time_slot = get_trips_by_time_slot(df)
# Time slots are represented by integers :
# 0 -> 0-3
# 1 -> 4-7
# 2 -> 8-11
# 3 -> 12-15
# 4 -> 16-19
# 5 -> 20-23
```

### Compute average distance travelled for each day of the week
```python
from nytraffic.transformation import compute_average_distance_by_day

distance_by_day = compute_average_distance_by_day(df)
```

## Example <a name="example"></a>
```python
from nytraffic.example import run_example

run_example(output_dir='output')
# Writes in current working directory by default
```