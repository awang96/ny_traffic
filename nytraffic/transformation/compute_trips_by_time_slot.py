from __future__ import annotations
from typing import TYPE_CHECKING


if TYPE_CHECKING:
    from pyspark.sql import DataFrame

import pyspark.sql.functions as F
from pyspark.sql.types import IntegerType

from .common import _with_pickup_day, PICKUP_DAY
from .column_names import PICKUP_DATETIME


PICKUP_TIME_SLOT = 'pickup_time_slot'
N_TRIPS_BY_DAY_AND_TIME_SLOT = 'n_trips_by_day_and_time_slot'
N_TRIPS_BY_TIME_SLOT = 'n_trips_by_time_slot'


@F.udf(returnType=IntegerType())
def _get_time_slot(pickup_datetime: str):
    """Return time slot
    0 -> 0-3
    1 -> 4-7
    2 -> 8-11
    3 -> 12-15
    4 -> 16-19
    5 -> 20-23
    """
    def check_is_in_time_slot(value: int,
                              starting_hour_: int) -> bool:
        return starting_hour_ <= value < starting_hour_ + time_slot_range

    hour = int(pickup_datetime.split(' ')[1].split(':')[0])
    first_hour, last_hour, time_slot_range = 0, 24, 4
    for time_slot, starting_hour in enumerate(range(first_hour, last_hour, time_slot_range)):
        if check_is_in_time_slot(hour, starting_hour):
            return time_slot


def _with_time_slot(df: DataFrame) -> DataFrame:
    return df.withColumn(PICKUP_TIME_SLOT, _get_time_slot(df[PICKUP_DATETIME]))


def _count_trips_by_time_slot_in_same_day(df: DataFrame) -> DataFrame:
    return df.groupBy(df[PICKUP_DAY], df[PICKUP_TIME_SLOT])\
        .agg(F.count(df[PICKUP_DAY]).alias(N_TRIPS_BY_DAY_AND_TIME_SLOT))


def get_trips_by_time_slot(df: DataFrame) -> DataFrame:
    df = _with_time_slot(df)
    df = _with_pickup_day(df)
    df = _count_trips_by_time_slot_in_same_day(df)
    return df.groupBy(df[PICKUP_TIME_SLOT]).agg(F.avg(df[N_TRIPS_BY_DAY_AND_TIME_SLOT]).alias(N_TRIPS_BY_TIME_SLOT))

