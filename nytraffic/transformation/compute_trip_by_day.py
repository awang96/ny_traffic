from __future__ import annotations
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from pyspark.sql import DataFrame

import pyspark.sql.functions as F

from .common import _with_pickup_day, _with_weekday, \
    PICKUP_DAY, WEEKDAY


N_TRIPS = 'n_trips'
AVG_TRIPS = 'avg_trips'


def get_trips_by_day(df: DataFrame) -> DataFrame:
    df = _with_pickup_day(df)
    df = _count_trips_by_day(df)
    df = _with_weekday(df)
    return df.groupby(df[WEEKDAY]).agg(F.avg(df[N_TRIPS]).alias(AVG_TRIPS))


def _count_trips_by_day(df: DataFrame) -> DataFrame:
    return df.groupBy(df[PICKUP_DAY]).agg(F.count(df[PICKUP_DAY]).alias(N_TRIPS))
