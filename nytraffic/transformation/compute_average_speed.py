from __future__ import annotations
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from pyspark.sql import DataFrame

import haversine
from pyspark.sql.functions import udf
from pyspark.sql.types import DoubleType

from .column_names import PICKUP_LATITUDE, PICKUP_LONGITUDE, DROPOFF_LATITUDE, DROPOFF_LONGITUDE, TRIP_DURATION


AVERAGE_SPEED = 'average_speed'


def with_average_speed(df: DataFrame) -> DataFrame:
    return df.withColumn(AVERAGE_SPEED,
                         _compute_average_speed(df[PICKUP_LATITUDE],
                                                df[PICKUP_LONGITUDE],
                                                df[DROPOFF_LATITUDE],
                                                df[DROPOFF_LONGITUDE],
                                                df[TRIP_DURATION]))


@udf(returnType=DoubleType())
def _compute_average_speed(pickup_latitude: float,
                           pickup_longitude: float,
                           dropoff_latitude: float,
                           dropoff_longitude: float,
                           trip_duration: int):
    """Compute average speed in m/s"""
    distance_in_m = haversine.haversine((pickup_latitude, pickup_longitude),
                                        (dropoff_latitude, dropoff_longitude),
                                        unit=haversine.Unit.METERS)
    return distance_in_m / trip_duration
