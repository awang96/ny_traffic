from __future__ import annotations
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from pyspark.sql import DataFrame

from datetime import datetime

import pyspark.sql.functions as F
from pyspark.sql.types import IntegerType

from .column_names import PICKUP_DATETIME


PICKUP_DAY = 'pickup_day'
WEEKDAY = 'weekday'


def _with_pickup_day(df: DataFrame) -> DataFrame:
    return df.withColumn(PICKUP_DAY, F.substring(df[PICKUP_DATETIME], 1, 10))


def _with_weekday(df: DataFrame) -> DataFrame:
    return df.withColumn(WEEKDAY, _get_weekday(df[PICKUP_DAY]))


@F.udf(returnType=IntegerType())
def _get_weekday(pickup_day: str):
    """Return weekday as integer (0 -> Monday ... 6 -> Sunday)"""
    pickup_day = datetime.strptime(pickup_day, '%Y-%m-%d')
    return pickup_day.weekday()
