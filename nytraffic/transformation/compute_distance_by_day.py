from __future__ import annotations
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from pyspark.sql import DataFrame

import pyspark.sql.functions as F
from pyspark.sql.types import DoubleType
import haversine

from nytraffic.transformation.column_names import PICKUP_LATITUDE, PICKUP_LONGITUDE,\
    DROPOFF_LATITUDE, DROPOFF_LONGITUDE, \
    PICKUP_DATETIME
from nytraffic.transformation.common import _with_pickup_day, _with_weekday, \
    PICKUP_DAY, WEEKDAY


DISTANCE_IN_KM = 'distance_in_km'
AVG_DISTANCE_IN_KM = 'avg_distance_in_km'
TOTAL_DISTANCE_KM_IN_DAY = 'total_distance_km_in_day'


@F.udf(returnType=DoubleType())
def _compute_distance(pickup_latitude: float,
                      pickup_longitude: float,
                      dropoff_latitude: float,
                      dropoff_longitude: float):
    return haversine.haversine((pickup_latitude, pickup_longitude),
                               (dropoff_latitude, dropoff_longitude),
                               unit=haversine.Unit.KILOMETERS)


def _with_distance_in_km(df: DataFrame) -> DataFrame:
    return df.withColumn(DISTANCE_IN_KM, _compute_distance(df[PICKUP_LATITUDE],
                                                           df[PICKUP_LONGITUDE],
                                                           df[DROPOFF_LATITUDE],
                                                           df[DROPOFF_LONGITUDE]))


def _compute_total_distance_in_a_day(df: DataFrame) -> DataFrame:
    return df.groupBy(df[PICKUP_DAY]).agg(F.sum(df[DISTANCE_IN_KM]).alias(TOTAL_DISTANCE_KM_IN_DAY))


def compute_average_distance_by_day(df: DataFrame) -> DataFrame:
    df = _with_distance_in_km(df)
    df = _with_pickup_day(df)
    print(df.count())
    df = _compute_total_distance_in_a_day(df)
    df = _with_weekday(df)
    print(df.count())
    return df.groupBy(df[WEEKDAY]).agg(F.avg(df[TOTAL_DISTANCE_KM_IN_DAY]).alias(AVG_DISTANCE_IN_KM))
