from __future__ import annotations
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from pyspark.sql import DataFrame

import os

from nytraffic.transformation import with_average_speed, get_trips_by_day, get_trips_by_time_slot, \
    compute_average_distance_by_day
from nytraffic.sparksession import spark


def run_example(output_dir: str = '.') -> None:
    def write_to_csv(df_: DataFrame,
                     output_file_name: str) -> None:
        df_.write\
            .format('csv')\
            .option('header', True)\
            .mode('overwrite')\
            .save(F'{output_dir}/{output_file_name}')

    df = spark.read\
            .options(header=True,
                     inferSchema=True)\
            .csv(F'{os.path.dirname(__file__)}/sample.csv')

    df_with_average_speed = with_average_speed(df)
    write_to_csv(df_with_average_speed, 'sample_with_average_speed')

    trips_by_day = get_trips_by_day(df)
    write_to_csv(trips_by_day, 'sample_trips_by_day')

    trips_by_time_slot = get_trips_by_time_slot(df)
    write_to_csv(trips_by_time_slot, 'sample_trips_by_time_slot')

    distance_by_day = compute_average_distance_by_day(df)
    write_to_csv(distance_by_day, 'sample_distance_by_day')
