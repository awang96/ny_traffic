from nytraffic.sparksession import spark
from nytraffic.transformation import get_trips_by_time_slot
from nytraffic.transformation.compute_trips_by_time_slot import PICKUP_TIME_SLOT, N_TRIPS_BY_TIME_SLOT
from nytraffic.transformation.column_names import PICKUP_DATETIME
from.pickup_datetime_source_data import PICKUP_DATETIME_DATA


def test_trips_by_time_slot_computed():
    source_data = PICKUP_DATETIME_DATA
    source_df = spark.createDataFrame(source_data, [PICKUP_DATETIME])

    actual_df = get_trips_by_time_slot(source_df)

    expected_data = [(1, 1.),
                     (3, 1.25),
                     (4, 1.5),
                     (2, 2.),
                     (0, 1.)]
    expected_df = spark.createDataFrame(expected_data,
                                        [PICKUP_TIME_SLOT, N_TRIPS_BY_TIME_SLOT])

    assert(expected_df.collect() == actual_df.collect())
