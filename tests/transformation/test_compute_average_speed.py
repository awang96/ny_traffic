import haversine

from nytraffic.sparksession import spark
from nytraffic.transformation import with_average_speed
from nytraffic.transformation.column_names import PICKUP_LATITUDE, PICKUP_LONGITUDE, DROPOFF_LATITUDE,\
    DROPOFF_LONGITUDE, TRIP_DURATION
from nytraffic.transformation.compute_average_speed import AVERAGE_SPEED


def test_average_speed_computed():
    source_data = [
        (48.83784913971432, 2.4834922540452404, 48.849074378793446, 2.3896590982228347, 600),
        (48.86267279163018, 2.287480043514196, 48.83816704168218, 2.246419782133649, 400)
    ]
    source_df = spark.createDataFrame(source_data,
                                      [PICKUP_LATITUDE,
                                       PICKUP_LONGITUDE,
                                       DROPOFF_LATITUDE,
                                       DROPOFF_LONGITUDE,
                                       TRIP_DURATION])

    actual_df = with_average_speed(source_df)

    expected_data = [
        source_data[0] + (haversine.haversine((source_data[0][0], source_data[0][1]),
                                              (source_data[0][2], source_data[0][3]),
                                              unit=haversine.Unit.METERS) / source_data[0][4],),
        source_data[1] + (haversine.haversine((source_data[1][0], source_data[1][1]),
                                              (source_data[1][2], source_data[1][3]),
                                              unit=haversine.Unit.METERS) / source_data[1][4],)
    ]
    expected_df = spark.createDataFrame(expected_data,
                                        [PICKUP_LATITUDE,
                                         PICKUP_LONGITUDE,
                                         DROPOFF_LATITUDE,
                                         DROPOFF_LONGITUDE,
                                         TRIP_DURATION,
                                         AVERAGE_SPEED])

    assert(expected_df.collect() == actual_df.collect())
