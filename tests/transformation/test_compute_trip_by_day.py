from nytraffic.sparksession import spark
from nytraffic.transformation import get_trips_by_day
from nytraffic.transformation.column_names import PICKUP_DATETIME
from nytraffic.transformation.compute_trip_by_day import WEEKDAY, AVG_TRIPS
from .pickup_datetime_source_data import PICKUP_DATETIME_DATA


def test_trips_by_day_computed():
    source_data = PICKUP_DATETIME_DATA
    source_df = spark.createDataFrame(source_data,
                                      [PICKUP_DATETIME])

    actual_df = get_trips_by_day(source_df)

    expected_data = [(1, 1.5),
                     (6, 1.),
                     (3, 1.),
                     (5, 1.),
                     (4, 3.),
                     (2, 1.),
                     (0, 2.)]
    expected_df = spark.createDataFrame(expected_data,
                                        [WEEKDAY, AVG_TRIPS])

    assert(expected_df.collect() == actual_df.collect())
